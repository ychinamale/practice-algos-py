#User function Template for python3
# https://practice.geeksforgeeks.org/problems/merge-sort/1

def main():
    arr = [4,8,2,5,1,0,2,9,3]
    solution = mergeSort(arr)

    print("### Solution is {}".format(solution))

def merge(leftSorted, rightSorted):
    i, j = 0, 0
    
    mergedList = []
    
    while i < len(leftSorted) and j < len(rightSorted):

        if leftSorted[i] < rightSorted[j]:
            mergedList.append(leftSorted[i])
            i += 1
        else:
            mergedList.append(rightSorted[j])
            j += 1

    # check if anything is left
    if i >= len(leftSorted) and j < len(rightSorted):
        mergedList += rightSorted[j:]
        
    elif j >= len(rightSorted) and i < len(leftSorted):
        mergedList += leftSorted[i:]
    
    return mergedList


def mergeSort(arr):

    if len(arr) > 1:
        # break into left and right parts
        lenArr = len(arr)
        mid = lenArr // 2
        leftPart = arr[:mid]
        rightPart = arr[mid:]

        # sort left part
        leftSorted = mergeSort(leftPart)
        
        # sort right part
        rightSorted = mergeSort(rightPart)
        
        # merge parts
        mergeSorted = merge(leftSorted, rightSorted)
        
        # return solution
        return mergeSorted
    else:
        return arr


if __name__ == "__main__":
    main()