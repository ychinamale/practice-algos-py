#!/usr/bin/env python3
'''
    Author:         Yamikani Chinamale
    Description:    Practicing binary search in python

    To do:         
                    - Return index of search result
                    - Return list of closest items, minimum of 3
                        - i.e. if next half will have less than 3 items, return list

'''
import random
import sys

def main():
    N = 51
    inputList = getRandomList(N)
    
    query = inputList[random.randint(0,N)]

    # get sorted list - with indices added
    sortedIndexList = sorted([ [ inputList[i], i] for i in range(len(inputList))], key = sortByItem )

    ########## return index of query ###############
    indexResult = indexSearch(sortedIndexList, query)
    print("{}".format(indexResult))
    ################################################


    ####### return list of 3 closest elements ######
    #result = closestSearch(sorted, query)
    ################################################

def sortByItem(someList):
    return someList[0]

def sortByIndex(someList):
    return someList[1]


def indexSearch(thisList, query):
    if len(thisList) > 1:
        mid = len(thisList) // 2
        if query == thisList[mid][0]:
            return thisList[mid]

        elif query < thisList[mid][0]:
            return indexSearch(thisList[:mid], query)

        elif query > thisList[mid][0]:
            return indexSearch(thisList[mid+1:], query)

    else:
        if query != thisList[0][0]:
            return "Index Not Found"
        else:
            return thisList

    return 0

def closestSearch(thisList, query):

    return 0
    

def getRandomList(N):
    randomList = [random.randint(1,1000) for _ in range(N)]
    return randomList

if __name__ == "__main__":
    main()
    pass