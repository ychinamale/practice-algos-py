#!/usr/bin/env python3
'''
    Author:         Yamikani Chinamale
    Description:    An array A consisting of N different integers is given. 
                    The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing. Goal is to find that missing element.
'''
def main():
    A = [1,2,3,4,5,6,7,8,9]
    # A = [1,2,4,5,6,7,8,9,10]
    # A = [2]
    # A = []
    
    solution = search( sorted(A) )
    print("Solution is {}".format(solution))
    return solution

def search( sortedList ):

    if len(sortedList) == 0:
        return 1

    firstElem = sortedList[0]

    if firstElem != 1:
        return 1
    
    # if last element missing
    lastElem = len(sortedList) + 1

    # lastElem = sortedList[-1:][0]

    idealSum = (lastElem * (lastElem + 1) )//2

    currentSum = sum(sortedList)

    return idealSum - currentSum

if __name__ == "__main__":
    main()
    pass