#!/usr/bin/env python3
import sys

fiboCache = {}

def getFibonacci(N):
    if N == 1:
        return 1
    elif N == 2:
        return 1
    else:
        if N in fiboCache:
            return fiboCache[N]
        else:
            result = getFibonacci(N-1) + getFibonacci(N-2)
            fiboCache[N] = result

            return result

    pass

def main():
    fiboList = []

    if len(sys.argv) < 2:
        print ("Usage:\n\tpython fibonacci <number>\n")
    else:
        N = int(sys.argv[1])
        solution = getFibonacci(N)
        print ("Fibonacci is {}".format(solution))

        for key, value in fiboCache.items():
            fiboList.append(value)

        fiboList = [1, 1] + fiboList
        print ("{}".format(fiboList))
    pass

if __name__ == "__main__":
    main()
    pass