#!/usr/bin/env python3
'''
    Author:             Yamikani Chinamale
    Description:        Given a list, all but one entry occur in pairs. Find the odd entry.
                        e.g. if A = [1,3,1,3,1,3,1,3,6,7,2,6,7,7,7,4,4], solution is 2.
'''
import inspect

def hasOtherMatch(midIndex, thisList):
    # check for out of bounds exception
    upperLimit = len(thisList) - 1
    lowerLimit = 0
    
    hasLowerMatch = False
    hasUpperMatch = False

    if midIndex+1 <= upperLimit:
        if thisList[midIndex] == thisList[midIndex+1]:
            hasUpperMatch = True

    if midIndex-1 >= lowerLimit:
        if thisList[midIndex] == thisList[midIndex-1]:
            hasLowerMatch = True

    if hasLowerMatch or hasUpperMatch:
        return True
    else:
        return False


def getLastLeftIndex(midIndex, thisList):
    lastIndex = midIndex
    item = thisList[midIndex]
    curIndex = midIndex

    while curIndex >= 0:
        if thisList[curIndex - 1] == item:
            lastIndex -= 1
            curIndex -= 1
        else:
            break

    return lastIndex

def getLastRightIndex(midIndex, thisList):
    lastIndex = midIndex
    item = thisList[midIndex]
    curIndex = midIndex

    while curIndex < len(thisList)-1:
        if thisList[curIndex + 1] == item:
            lastIndex += 1
            curIndex += 1
        else:
            break

    return lastIndex

def getSplitRight(rightIndex, sortedList):
    return sortedList[rightIndex+1:]

def getSplitLeft(leftIndex, sortedList):
    return sortedList[:leftIndex]

def search(sortedList):
    L = len(sortedList)
    if L == 1:
        return sortedList[0]
    
    midIndex = L//2

    if hasOtherMatch(midIndex, sortedList) == False:
        return sortedList[midIndex]

    # else continue
    lastLeftIndex = getLastLeftIndex(midIndex, sortedList)
    lastRightIndex = getLastRightIndex(midIndex, sortedList)
    rightSplit = []
    leftSplit = []

    if lastRightIndex < L-1:
        rightSplit = getSplitRight(lastRightIndex, sortedList)

    if lastRightIndex > 0:
        leftSplit = getSplitLeft(lastLeftIndex, sortedList)

    if len(rightSplit) % 2 != 0:
        return search( rightSplit )
    else:
        return search( leftSplit )
    pass

def main():
    A = [1,3,1,3,1,3,1,3,6,7,2,6,7,7,7,4,4]
    solution =  search( sorted(A) )
    print("solution is {}".format(solution))

def codeStub():
    print("{} is incomplete".format(inspect.stack()[1].function))

if __name__ == "__main__":
    main()
    pass